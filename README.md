# Miscellaneous tools

This repository contains small programs or scripts that do not deserve their own repositories, and are not R scripts or bioinformatics tools.


# quicknotes.sh
One liner to append a quick note to a specified markdown file, from command line. Obsidian is a note-taking app but it takes a few seconds to launch. If I am currently 
working in terminal and do not have Obsidian open, it is super helpful to just write my note in terminal and still have it appear in my Obsidian inbox/quick notes later.

# servcp.sh (incomplete)
The adress to the course server is very long and sometimes hard to remember and get right. This script is intended to be used as an alias so that it can be called with a 
file path (relative to server home) only.

# insp-wall.sh
This bash script reads quotes and their authors from a text file, and for each quote makes a black wallpaper with the quote in white text on it. 
To use, you need to install ImageMagick (https://imagemagick.org/index.php). 
The way I store my quotes is in plain text, with a quote on one line, the author on the line below it, then an empty line, then the next quote and so on. 
The script works with this input format, so make sure you structure yours the same way. 
See the text file sample-quotes.txt (Resources/) for the correct format. You can also use that file to test the script sraight away. 

