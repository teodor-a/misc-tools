#! /bin/bash

####### Description:
# Extremely short program that enables me to super quickly note a thought
# from command line, appended to my inbox/quick notes file in obsidian.
# Obsidian takes a few seconds to launch, so in those cases where I am not
# already in Obsidian I want to be able to quickly add thoughts that end
# up in obsidian anyway.
# It is highly suggested you add the program to your path,
# as well as as an alias. Do this by:
# 1. Open ~/.bashrc
# 2. Add the line 'export PATH="$PATH:path-to-this-script"
# 3. Add the line 'alias qn="quicknote.sh"
# (I chose 'qn' as my alias, you can choose whatever you want and find easy)
# 4. Save then source .bashrc

####### Usage:
# ./quicknote.sh This is the note I want to quickly jot down.
# Or, if you created an alias:
# qn This is the note I want to quickly jot down. 

####### The program:
# The path to the file which is the quick notes file:
#filepath=~/pCloudDrive/obsidian-zettelkasten/'🗒️quick notes.md'
# Obs! Specify variables withot quotes. This is how bash saves paths in vars.
# Change this path depending on you file name and location.
# Obs again. The above is commented out because it does not work. I tried saving
# path to a variable liek above and then refer to the variable below instead of the full path
# (to avoid repeating myself)
# BUT it does not work.. I think because of the space and/or emoji in the file name??


# First of all, I want to be able to call the program without arguments in order to 
# quickly check the contents of the quick notes file:
if [ $# -eq 0 ]
	then
		cat ~/pCloudDrive/obsidian-zettelkasten/'🗒️quick notes.md'
	else
		echo -e "\n\n$@" >> ~/pCloudDrive/obsidian-zettelkasten/'🗒️quick notes.md'
		#echo $@ >> ~/pCloudDrive/obsidian-zettelkasten
fi

# Then, if there were arguments (consisting of a quick thought), echo them to the file:
# echo $@ >> ~/pCloudDrive/obsidian-zettelkasten/🗒️quick\ notes.md
# It echoes all the arguments passed, starting from the first.
# Then it pipes the message appending it to the file I specified,
# in my case this is my quick notes file in my obsidian vault.
# (The arguments = the note you have written)
# That is it. There is no other function.
